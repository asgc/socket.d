colorama==0.4.6
connect==0.2
loguru==0.7.2
nest-asyncio==1.5.8
websockets==12.0
